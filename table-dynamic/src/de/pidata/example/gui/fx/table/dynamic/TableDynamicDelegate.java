package de.pidata.example.gui.fx.table.dynamic;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.guidef.DialogType;
import de.pidata.gui.view.base.*;
import de.pidata.models.binding.AttributeBinding;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

public class TableDynamicDelegate implements DialogControllerDelegate {

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, ParameterList parameterList ) throws Exception {
    DialogType root = (DialogType) XmlReader.loadData( "gui/table_dynamic.xml" );
    addTableColumns( dlgCtrl );
    return root;
  }

  private void addTableColumns( DialogController dialogController ) {
    ControllerBuilder ctrlBuilder = dialogController.getControllerBuilder();
    ViewFactory viewFactory = ctrlBuilder.getViewFactory();

    TableController tableController = (TableController) dialogController.getController( GuiBuilder.NAMESPACE.getQName( "tableCtrl" ) );
    NamespaceTable namespaceTable = dialogController.getNamespaceTable();
    for (int colIndex = 2; colIndex < 3; colIndex++) {
      QName colName = GuiBuilder.NAMESPACE.getQName( "Dynamic_"+colIndex );

      QName rendererID = GuiBuilder.NAMESPACE.getQName( "dynamicCell_"+colIndex );
      QName cellCompID = GuiBuilder.NAMESPACE.getQName( "multicell_comp" );
      ModuleController columnRenderer = new ModuleController();
      ModuleViewPI moduleViewPI = viewFactory.createModuleView( cellCompID );
      columnRenderer.init( rendererID, dialogController, moduleViewPI, null, null, false );
      ModuleGroup moduleGroup = new ModuleGroup( );
      moduleGroup.init( rendererID, columnRenderer, null, false, null );
      columnRenderer.setModuleGroup( moduleGroup, ViewAnimation.NONE );

      QName colReadOnly = GuiBuilder.NAMESPACE.getQName( "readOnly" );
      QName flagID = GuiBuilder.NAMESPACE.getQName( "cellCheck" );
      FlagController flagController = new FlagController();
      FlagViewPI flagComp = viewFactory.createFlagView( flagID, false );
      Binding flagBinding = ctrlBuilder.createAttributeBinding( dialogController, tableController, ".", colReadOnly, namespaceTable );
      flagController.init( flagID, moduleGroup, flagBinding, null, flagComp, null, false );
      moduleGroup.addController( flagID, flagController );

      QName colID = GuiBuilder.NAMESPACE.getQName( "ID" );
      QName textID = GuiBuilder.NAMESPACE.getQName( "cellText" );
      TextController textController = new TextController();
      TextViewPI textViewPI = viewFactory.createTextView( textID );
      Binding textBinding = ctrlBuilder.createAttributeBinding( dialogController, tableController, ".", colID, namespaceTable );
      textController.init( textID, moduleGroup, textViewPI, textBinding, true );
      moduleGroup.addController( textID, textController );

      QName btnID = GuiBuilder.NAMESPACE.getQName( "cellButton" );
      ButtonController buttonController = new ButtonController();
      ButtonViewPI buttonViewPI = viewFactory.createButtonView( btnID, null );
      GuiOperation op = new CellActionOp();
      buttonController.init( btnID, moduleGroup, buttonViewPI, null, null, null, op );
      moduleGroup.addController( btnID, buttonController );

      QName bodyCompID = GuiBuilder.NAMESPACE.getQName( "multicell_comp"+colIndex );
      ColumnInfo columnInfo = new ColumnInfo( dialogController.getContext(), colIndex, colName, ".", ".",
          null, null, null, null,
          null, bodyCompID, null, namespaceTable, columnRenderer, columnRenderer, true, false );
      tableController.addColumn( columnInfo );

      colIndex++;
    }

  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    //do nothing
  }

  @Override
  public void popupClosed( ModuleGroup oldModuleGroup) {
    //do nothing
  }

  public TableController getTableCtrl( DialogController dialogController ) {
    return (TableController) dialogController.getController( ControllerBuilder.NAMESPACE.getQName( "tableCtrl" ) );
  }
}

