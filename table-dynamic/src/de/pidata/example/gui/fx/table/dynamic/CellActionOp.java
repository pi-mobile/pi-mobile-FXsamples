package de.pidata.example.gui.fx.table.dynamic;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public class CellActionOp extends GuiDelegateOperation<TableDynamicDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID   the id of the event, see method description
   * @param delegate  the delegate of the DialogController of which ctrlGroup is part of
   * @param source    original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, TableDynamicDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    Boolean flag = (Boolean) dataContext.get( GuiBuilder.NAMESPACE.getQName( "readOnly" ) );
    showMessage( source, "Cell Action", "Pressed button in row ID="+dataContext.get( GuiBuilder.NAMESPACE.getQName( "ID" ) )+", flag="+flag );
  }
}
