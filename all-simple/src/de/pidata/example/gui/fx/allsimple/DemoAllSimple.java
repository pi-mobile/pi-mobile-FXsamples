package de.pidata.example.gui.fx.allsimple;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.guidef.ControllerFactory;
import de.pidata.gui.javafx.FXApplication;
import de.pidata.models.tree.BaseFactory;
import de.pidata.models.tree.ModelFactoryTable;

public class DemoAllSimple extends FXApplication {

  /**
   * The application initialization method. This method is called immediately
   * after the Application class is loaded and constructed. An application may
   * override this method to perform initialization prior to the actual starting
   * of the application.
   * <p>
   * <p>
   * The implementation of this method provided by the Application class does nothing.
   * </p>
   * <p>
   * <p>
   * NOTE: This method is not called on the JavaFX Application Thread. An
   * application must not construct a Scene or a Stage in this
   * method.
   * An application may construct other JavaFX objects in this method.
   * </p>
   */
  @Override
  public void init() throws Exception {
    Platform.loadServices = false;
    ModelFactoryTable factoryTable = ModelFactoryTable.getInstance();
    factoryTable.getOrSetFactory( BaseFactory.NAMESPACE, BaseFactory.class );
    factoryTable.getOrSetFactory( ControllerFactory.NAMESPACE, ControllerFactory.class );
    super.init();
  }

  public static void main( String[] args ) {
    args = new String[1];
    args[0] = "all_simple_app";
    launch( args );
  }
}
