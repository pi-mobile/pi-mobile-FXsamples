package de.pidata.example.gui.fx.allsimple;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.FlagController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.crypt.DefaultSHA1Encrypter;
import de.pidata.models.crypt.Encrypter;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SayHello extends GuiOperation {

  private static final String HELLO1 = "hello world";
  private static final String HELLO2 = "hello again";

  private static final DecimalObject HELLO_NUMBER1 = new DecimalObject( "42,24", InputManager.DECIMAL_SEPARATOR, InputManager.THOUSAND_SEPARATOR );
  private static final DecimalObject HELLO_NUMBER2 = new DecimalObject( "24,42", InputManager.DECIMAL_SEPARATOR, InputManager.THOUSAND_SEPARATOR );
  private static final Integer HELLO_INTEGER1 = 42;
  private static final Integer HELLO_INTEGER2 = 24;

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID   the id of the event, see method description
   * @param sourceCtrl    original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Logger.info( "called SayHello..." );
    DialogController dialogController = sourceCtrl.getDialogController();

    List<String> ctrlNames = Arrays.asList( "demoText", "demoTextEditor", "demoCodeEditor", "demoWebView", "demoHtmlEditor" );
    List<Controller> ctrlList = new ArrayList<>();
    for (String name : ctrlNames) {
      Controller ctrl = dialogController.getController( ControllerBuilder.NAMESPACE.getQName( name ) );
      if (ctrl != null) {
        ctrlList.add( ctrl );
      }
      else {
        Logger.warn( "controller not found: " + name );
      }
    }
    for (Controller controller : ctrlList) {
      toggleValue( controller, HELLO1, HELLO2 );
    }

    Controller ctrl;
    ctrl = dialogController.getController( ControllerBuilder.NAMESPACE.getQName( "demoPassword" ) );
    if (ctrl != null) {
      try {
        Encrypter encrypter = new DefaultSHA1Encrypter();
        Object value = ctrl.getValue();
        String value1 = encrypter.encrypt( HELLO1 );
        String value2 = encrypter.encrypt( HELLO2 );
        if (value1.equals( value )) {
          Logger.info( "encrypted 1: " + value );
          ctrl.setValue( value2 );
        }
      }
      catch (NoSuchAlgorithmException e) {
        Logger.error( "error in encrypt", e );
      }
    }
    ctrl = dialogController.getController( ControllerBuilder.NAMESPACE.getQName( "demoNumber" ) );
    if (ctrl != null) {
      toggleValue( ctrl, HELLO_NUMBER1, HELLO_NUMBER2 );
    }
    ctrl = dialogController.getController( ControllerBuilder.NAMESPACE.getQName( "demoInteger" ) );
    if (ctrl != null) {
      toggleValue( ctrl, HELLO_INTEGER1, HELLO_INTEGER2 );
    }
    ctrl = dialogController.getController( ControllerBuilder.NAMESPACE.getQName( "demoCheckbox" ) );
    if (ctrl != null) {
      toggleCheckboxValue( ((FlagController) ctrl) );
    }
    ctrl = dialogController.getController( ControllerBuilder.NAMESPACE.getQName( "demoCheckboxTristate" ) );
    if (ctrl != null) {
      toggleTristateValue( ((FlagController) ctrl) );
    }
  }

  private void toggleCheckboxValue( FlagController ctrl ) {
    Object value = ctrl.getValue();
    ctrl.setValue( !((Boolean) value) );
  }

  private void toggleTristateValue( FlagController ctrl ) {
    Object value = ctrl.getValue();
    if (value == null) {
      ctrl.setValue( true );
    }
    else if (((Boolean) value)) {
      ctrl.setValue( false );
    }
    else {
      ctrl.setValue( null );
    }
  }

  private void toggleValue( Controller ctrl, Object value1, Object value2 ) {
    Object value = ctrl.getValue();
    if (value1.equals( value )) {
      ctrl.setValue( value2 );
    }
    else {
      ctrl.setValue( value1 );
    }
  }
}
