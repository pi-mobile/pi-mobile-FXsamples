package de.pidata.example.gui.fx.table.htmledit;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.FXApplication;
import de.pidata.log.Level;
import de.pidata.log.Logger;
import javafx.stage.Stage;

/**
 * Created by pru on 14.07.16.
 */
public class TableEdit extends FXApplication {

  /**
   * The application initialization method. This method is called immediately
   * after the Application class is loaded and constructed. An application may
   * override this method to perform initialization prior to the actual starting
   * of the application.
   * <p>
   * <p>
   * The implementation of this method provided by the Application class does nothing.
   * </p>
   * <p>
   * <p>
   * NOTE: This method is not called on the JavaFX Application Thread. An
   * application must not construct a Scene or a Stage in this
   * method.
   * An application may construct other JavaFX objects in this method.
   * </p>
   */
  @Override
  public void init() throws Exception {
    Platform.loadServices = false;
    super.init();
  }

  @Override
  public void start(Stage primaryStage ) throws Exception {
    primaryStage.setX( 0.0 );
    primaryStage.setY( 0.0 );
    super.start( primaryStage );
  }

  public static void main( String[] args ) {
    args = new String[1];
    args[0] = "table_edit_app";
    Logger.setLogLevel( Level.DEBUG );
    launch( args );
  }
}
