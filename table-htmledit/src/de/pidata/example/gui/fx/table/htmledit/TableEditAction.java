package de.pidata.example.gui.fx.table.htmledit;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.TableController;
import de.pidata.service.base.ServiceException;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

/**
 * Created by pru on 14.07.16.
 */
public class TableEditAction extends GuiDelegateOperation<TableEditDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID   the id of the event, see method description
   * @param delegate  the delegate of the DialogController of which ctrlGroup is part of
   * @param source    original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, TableEditDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    TableController treeCtrl = delegate.getTableCtrl( source.getDialogController() );
    Model selectedModel = treeCtrl.getSelectedRow(0);
    if (selectedModel == null) {
      showMessage( source, "Selected", "No selected row" );
    }
    else {
      String nodeLabel = treeCtrl.getView().getCellValue( selectedModel, treeCtrl.getColumn( (short) 0 ).getValueID() ).toString();
      showMessage( source, "Selected", "Selected row: " + nodeLabel );
    }
  }
}
