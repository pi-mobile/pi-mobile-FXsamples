package de.pidata.example.gui.fx.figures.test;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.PaintController;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.gui.view.figure.*;
import de.pidata.rect.*;
import de.pidata.models.tree.Model;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pru on 09.06.17.
 */
public class TestFiguresDelegate  implements DialogControllerDelegate {

  private PaintController paintController;

  /**
   * Gives this delegate the chance to set or modify the dialog model.
   * In simple cases just return the given parameter.
   * This method is called as last step of DialogController's init method,
   * which means after having processed cloneModel and modelPath and before
   * initializing child controllers.
   *
   * @param dlgCtrl       the calling dialog controller
   * @param parameterList list of parameters for this delegate's dialog
   * @return the model to use
   */
  @Override
  public Model initializeDialog( DialogController dlgCtrl, ParameterList parameterList ) throws Exception {
    paintController = (PaintController) dlgCtrl.getController( GuiBuilder.NAMESPACE.getQName( "figures" ) );
    PaintViewPI paintViewPI = (PaintViewPI) paintController.getView();

    ShapeStyle style = new ShapeStyle( ComponentColor.GREEN, ComponentColor.ORANGE );
    SimpleFigure simpleFigure = new SimpleFigure( ShapeType.rect, new SimpleRect( 30,20, 70, 50 ), style );
    paintViewPI.addFigure( simpleFigure );

    Rect ellipseRect = new SimpleRect( 130,20, 70, 50 );
    List<ComponentColor> colorList = new ArrayList<>(  );
    List<Double> offsetList = new ArrayList<>(  );
    colorList.add( Platform.getInstance().getColor( ComponentColor.YELLOW ) );
    offsetList.add( 0.0 );
    colorList.add( Platform.getInstance().getColor( ComponentColor.RED ) );
    offsetList.add( 0.7 );
    CircleGradientStyle circleGradientStyle = new CircleGradientStyle( RectRelatedPos.createCenter( ellipseRect ), 50, colorList, offsetList, GradientMode.CLAMP );
    simpleFigure = new SimpleFigure( ShapeType.ellipse, ellipseRect, circleGradientStyle );
    paintViewPI.addFigure( simpleFigure );

    Rect gradientRect = new SimpleRect( 300, 80, 70, 50 );
    List<ComponentColor> colorLinearList = new ArrayList<>(  );
    List<Double> offsetLinearList = new ArrayList<>(  );
    colorLinearList.add( Platform.getInstance().getColor( ComponentColor.BLUE ) );
    offsetLinearList.add( 0.0 );
    colorLinearList.add( Platform.getInstance().getColor( ComponentColor.GREEN ) );
    offsetLinearList.add( 0.7 );
    LinearGradientStyle linearGradientStyle = new LinearGradientStyle( RectRelatedPos.createCenterTop( gradientRect ), RectRelatedPos.createCenterBottom( gradientRect ), colorLinearList, offsetLinearList, GradientMode.CLAMP );
    simpleFigure = new SimpleFigure( ShapeType.rect, gradientRect, linearGradientStyle );
    paintViewPI.addFigure( simpleFigure );

    RectRelatedRect rectRelatedRect = new RectRelatedRect( gradientRect, 0, 0, 0, -40, 100, 40 );
    simpleFigure = new SimpleFigure( ShapeType.rect, rectRelatedRect, new ShapeStyle( ComponentColor.PINK ) );
    paintViewPI.addFigure( simpleFigure );

    Rect ellipseRelRect = new SimpleRect( 180,120, 70, 100 );
    RectRelatedPos gradientCenter = new RectRelatedPos( gradientRect, 0, -(gradientRect.getX() - (130 + 80)), 0.2, 50 );
    CircleGradientStyle circleRefGradientStyle = new CircleGradientStyle( gradientCenter, 50, colorList, offsetList, GradientMode.CLAMP );
    simpleFigure = new SimpleFigure( ShapeType.ellipse, ellipseRelRect, circleRefGradientStyle );
    paintViewPI.addFigure( simpleFigure );

    Rotation rotation = new Rotation( null,30, new SimplePos( 70, 100) );
    simpleFigure = new SimpleFigure( ShapeType.rect, new SimpleRectDir(70,100, 70, 50, rotation ), ShapeStyle.STYLE_BLACK );
    paintViewPI.addFigure( simpleFigure );

    Rect bounds = new SimpleRect( 80,150, 70, 50 );
    Pos[] positions = new Pos[5];
    positions[0] = new RelativePos( bounds, 0, 50 );
    positions[1] = new RelativePos( bounds, 30, 0 );
    positions[2] = new RelativePos( bounds, 60, 0 );
    positions[3] = new RelativePos( bounds, 70, 50 );
    positions[4] = new RelativePos( bounds, 0, 50 );
    style = new ShapeStyle( ComponentColor.BLUE, ComponentColor.TRANSPARENT );
    simpleFigure = new SimpleFigure( ShapeType.path, bounds, style, positions );
    paintViewPI.addFigure( simpleFigure );

    SimpleRect textRect = new SimpleRect( 350, 250, 200, 50 );
    ShapeStyle textRectStyle = new ShapeStyle( ComponentColor.RED, ComponentColor.GREEN );
    textRectStyle.setFillColor( ComponentColor.TRANSPARENT );
    simpleFigure = new SimpleFigure( ShapeType.rect, textRect, textRectStyle );
    paintViewPI.addFigure( simpleFigure );
    ShapeStyle textStyle = new ShapeStyle( ComponentColor.RED, "Verdana", TextAlign.RIGHT );
    simpleFigure = new SimpleFigure( ShapeType.text, textRect, textStyle, "Hugo" );
    paintViewPI.addFigure( simpleFigure );

//    ShapeStyle arcStyle = new ShapeStyle( ComponentColor.BLACK, ComponentColor.LIGHTGRAY, 5 );
//    double radius = 50;
//    Rect arcRect = new SimpleRect( 30, 250, 2*radius, 2*radius );
//    simpleFigure = new SimpleFigure( ShapeType.rect, arcRect, new ShapeStyle( ComponentColor.BLUE, ComponentColor.MAGENTA, 2 ) );
//    paintViewPI.addFigure( simpleFigure );
//    Pos apexPos = new SimplePos( arcRect.getX() + radius, arcRect.getY() + radius );
//    simpleFigure = new SimpleFigure( ShapeType.path, apexPos, radius, 0, 30, arcStyle );
//    paintViewPI.addFigure( simpleFigure );

    return null;
  }

  /**
   * Called by dialog controller after having finished creating the dialog, i.e. when
   * all children have been added and before refreshBindings() is called.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogCreated( DialogController dlgCtrl ) {
  }

  /**
   * Called by dialog controller after all bindings have been initialized.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogBindingsInitialized( DialogController dlgCtrl ) {
  }

  /**
   * Called by dialog controller when the dialog is showing, i.e. the dialog is
   * visible for the user.
   *
   * @param dlgCtrl the calling dialog controller
   */
  @Override
  public void dialogShowing( DialogController dlgCtrl ) {
  }

  /**
   * Called by dlgCtrl on Platforms having a special "Back-Button" (e.g. Android)
   * if that Button is pressed.
   *
   * @param dlgCtrl the dlgController on which the back button was pressed
   */
  @Override
  public void backPressed( DialogController dlgCtrl ) {
    dlgCtrl.close( false );
  }

  /**
   * Called by dialog controller just before the dialog is closed
   * and after all controllers have been deactivated.
   *
   * @param dlgCtrl the calling dialog controller
   * @param ok
   */
  @Override
  public ParameterList dialogClosing( DialogController dlgCtrl, boolean ok ) {
    return AbstractParameterList.EMPTY;
  }

  @Override
  public void dialogClosed( DialogController dlgCtrl, boolean resultOK, ParameterList resultList ) {
    //do nothing
  }

  @Override
  public void popupClosed(ModuleGroup oldModuleGroup) {
    //do nothing
  }
}
