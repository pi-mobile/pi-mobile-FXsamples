package de.pidata.example.gui.fx.hello;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public class SayHello extends GuiOperation {

  private static final String HELLO1 = "hello world";
  private static final String HELLO2 = "hello again";

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID   the id of the event, see method description
   * @param sourceCtrl    original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    Logger.info( "called SayHello..." );
    DialogController dialogController = sourceCtrl.getDialogController();
    Controller helloTextCtrl = dialogController.getController( ControllerBuilder.NAMESPACE.getQName( "helloText" ) );
    Object value = helloTextCtrl.getValue();
    if (HELLO1.equals( value )) {
      helloTextCtrl.setValue( HELLO2 );
    }
    else {
      helloTextCtrl.setValue( HELLO1 );
    }
  }
}
