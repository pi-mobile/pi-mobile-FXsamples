package de.pidata.example.gui.fx.tree.add;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.service.base.ServiceException;
import de.pidata.qnames.QName;

/**
 * Created by pru on 14.07.16.
 */
public class TestTreeAction extends GuiDelegateOperation<TestTreeDelegate> {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID   the id of the event, see method description
   * @param delegate  the delegate of the DialogController of which ctrlGroup is part of
   * @param source    original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  protected void execute( QName eventID, TestTreeDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    TreeController treeCtrl = delegate.getTreeCtrl( source.getDialogController() );
    TreeNodePI selectedNode = treeCtrl.getSelectedNode();
    if (selectedNode == null) {
      showMessage( source, "Selected", "No selected node" );
    }
    else {
      String nodeLabel = selectedNode.getNodeModel().get( selectedNode.getDisplayValueID() ).toString();
      showMessage( source, "Selected", "Selected node: " + nodeLabel );
    }
  }
}
