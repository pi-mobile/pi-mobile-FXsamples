package de.pidata.example.gui.fx.tree.add;

import de.pidata.example.tree.data.Folder;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiDelegateOperation;
import de.pidata.gui.controller.base.TreeController;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public class TestTreeNewNodeAction extends GuiDelegateOperation<TestTreeDelegate> {

  @Override
  protected void execute( QName eventID, TestTreeDelegate delegate, Controller source, Model dataContext ) throws ServiceException {
    TreeController treeCtrl = delegate.getTreeCtrl( source.getDialogController() );
    TreeNodePI selectedNode = treeCtrl.getSelectedNode();
    if (selectedNode == null) {
      showMessage( source, "Selected", "No selected node" );
    }
    else {
      Folder selectedFolder = (Folder) selectedNode.getNodeModel();
      Folder newChild =  new Folder( -1, "unnamed", true, true, 0 );
      selectedFolder.addChildFolder( newChild );
      TreeNodePI newChildNode = selectedNode.findNode( newChild );
      treeCtrl.editNode( newChildNode );
    }
  }
}
