# PI-Mobile-Examples

Project containing a lot of examples showing how to use PI-Mobile.

## Getting started

1. Create a directory for your PI-Mobile projects
1. Check out all PI-Mobile projects to that directory.
   Each project should be located in a directory having the same name like the repository.
   You need the following directories (repositories):
   - [pi-mobile-core](https://gitlab.com/pi-mobile/pi-mobile-core)
   - [pi-mobile-fx](https://gitlab.com/pi-mobile/pi-mobile-fx)
   - [pi-mobile-FXsamples](https://gitlab.com/pi-mobile/pi-mobile-FXsamples)
1. Open "pi-mobile-FXsamples" project with IntelliJ IDEA.

Now you can run the examples and play with the sources.

For modifying the data structures see the ant tasks.

## How to contribute?

There are many ways you can help this project. There is a lot of work to do:
- Enhance documentation in source code and doc module
- Testing and Bug reporting
- Adding further examples as new modules to this project
